describe('Page Liste des randonnées', ()=> {
    it('Affichage des randonnées en ligne', ()=> {
        expect(element(by.id('item_rando')));
    });

    it('Affichage des randonnées hors-ligne', ()=> {
        expect(element(by.id('error_elem'))).getText().toEqual('Erreur connexion internet');
    });

    it('Navigation vers la page détail', ()=> {
        element.(by.id('item_rando')).click();
        expect(element(by.model('map')));
    });

    it('Navigation vers la page rando en cours', ()=> {
        element.(by.model('timer')).click();
        expect(element(by.model('map')));
    });
})

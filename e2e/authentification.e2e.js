describe('Page d’authentification', ()=> {
    it('L’utilisateur se connecte avec de bons identifiants', ()=> {
      element(by.id('username')).sendKeys('admin');
      element(by.id('password')).sendKeys('admin');
      element.(by.id('login_Btn')).click();
      expect(element(by.id('title_liste'))).getText().toEqual('Mes Randonnées');
    });

    it('L’utilisateur se connecte avec de mauvais identifiants', ()=> {
      element(by.id('username')).sendKeys('efzerezr');
      element(by.id('password')).sendKeys('fggdddd');
      element.(by.id('login_Btn')).click();
      expect(element(by.class('alert-head')));
    });
})

# ionic-appRando Présentation
Binôme de projet :
  Alexandre Caillot
  Florian Hebert

#Travail effectué :

[x] L'application fonctionne sous IOS et Android ainsi que sur navigateur
[x] Decoupage modulaire
[x] Creation de service injectable
[x] Creation de nos propre composant (Map, Footer ,Timer)
[x] Design en accord avec les maquettes
[x] Page de login
 - [x] Pop up error si mauvais login
[x] Liste des randonnées
 - [x] Indication si pas de connection pour récuperer les données
[x] Récupération des randonnées via une requête HTTP
depuis l'API https://data.issy.com/api/
[x] Détail pour chaque randonnées
[x] Affichage de toutes les étapes pour les randonnées à l'aide de l'API Google Map Directions Service
[x] Possibilité de lancer une randonnées
[x] Lancement du timer lors du départ et données du timer injecter dans le service dédier
( permet l'affichage de la rando courrante et son timer en cours depuis la liste des rando)
[x] La randonnées s'affiche sur la carte (avec son tracet mais aussi un curseur de notre position courante)
[x] Suscribe avec geolocation pour le rafraichissement des coordonnées
[x] Possibilité de stoper et finir la randonnées
[x] Option dans le menu pour consulté la randonnées en cour

#Ce qui est en cours de réalisation
[/] Test unitaire et fonctionnel (Config protractor OK et test fonctionnel OK pour page login et liste Rando)
[/] Gestion des collisions avec les checkpoints des randonnées
  -> intégrations avec Google Map API - Utilsations Circles google map


#Bonus
--------

#Répartition tâches
La répartition du boulot fut équitable tout au long du projet 50/50
( une seule personne à mis à jour le Git car le travail s'est fait principalement sur 1 PC )
Alexandre Caillot
  Mise en place structurel des services (RandoFactory, RandoService, HttpService)
  Récupération des données via HTTP
  co-Realisation component (Map, Footer ,Timer)
  co-Mise en place geolocation + google map API
Hebert Florian
  Réalisations des pages (design, controller typescript)
  Mise en place structurel des services (TimerService, MapService)
  co-Realisation component (Map, Footer ,Timer)
  Mise en place test fonctionnel
  co-Mise en place geolocation + google map API

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Model
import { Rando } from '../../entity/Rando/Rando';

//Controller
import { ActiveRando } from '../activeRando/activeRando';

// Services
import { RandoService } from '../../service/Rando/RandoService';
import { TimerService } from '../../service/Timer/TimerService';

// --------------------------------
// Controller : Home
@Component({
  selector: 'detail',
  templateUrl: 'detail.html'
})
export class DetailPage {
  private rando: Rando;

  constructor(public navCtrl: NavController, public navParams: NavParams, public randoService: RandoService, public timerService: TimerService) {
    this.rando = navParams.get('rando');

    if (this.rando != this.randoService.getRando()) {
      this.timerService.initTimer(true);
    }

    this.randoService.setRando(this.rando);
  }

  getRando(): Rando {
    return this.rando;
  }

  startRando() {    
    this.randoService.setTimerActive(true);
    this.navCtrl.push(ActiveRando);
  }
}

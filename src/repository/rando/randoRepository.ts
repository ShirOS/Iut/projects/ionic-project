import { Injectable } from '@angular/core';

// Model
import { Rando } from '../../entity/Rando/Rando';

// Service
import { HttpService } from '../../service/Http/HttpService';

// --------------------------------
// Repository : Rando

@Injectable()
export class RandoRepository {
  protected randoList: Array<Rando>;

  constructor(public httpService: HttpService) {
    this.randoList = this.httpService.fetch();

    /*
    this.randoList.push(RandoFactory.generate('Rando 1'));
    this.randoList.push(RandoFactory.generate('Rando 2'));
    */
  }

  public findAll(): Array<Rando> {
    return this.randoList;
  }
}

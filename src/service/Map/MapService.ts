import { Injectable, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

// Model
import { Rando } from '../../entity/Rando/Rando';

// Constant
declare let google;

// --------------------------------
// Service : MapService

@Injectable()
export class MapService {
  protected rando:Rando;
  protected geolocation: Geolocation;
  protected map: any;
  protected markerPosition: any;

  protected startPoint: any;
  protected endPoint: any;
  protected traces: Array<any>;

  constructor(geolocation: Geolocation) {
    this.geolocation = geolocation;
  }

  public prepareTraces(rando: Rando) {
    if (this.rando !== rando) {
      this.rando = rando;

      this.traces = [];

      let start = this.rando.getPoints().shift();
      this.startPoint = new google.maps.LatLng(
        start.getLatitude(),
        start.getLongitude()
      );

      let end = this.rando.getPoints().pop();
      this.endPoint = new google.maps.LatLng(
        end.getLatitude(),
        end.getLongitude()
      );

      let count = 0;
      this.rando.getPoints().forEach((point) => {
        if (count > 22) { return; }

        let position = new google.maps.LatLng(
          point.getLatitude(),
          point.getLongitude()
        );

        this.traces.push({
          location: position,
          stopover: true
        });

        count++;
      });
    }
  }

  protected initMap(mapElement: ElementRef) {
    if (typeof this.rando != "undefined"
      && typeof this.startPoint != "undefined"
      && typeof this.endPoint != "undefined"
      && typeof this.traces != "undefined"
      && typeof mapElement != "undefined"
      && this.traces.length > 0
    ) {

      let mapOptions = {
        center: this.startPoint,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.TERRAIN
      }

      this.map = new google.maps.Map(mapElement.nativeElement, mapOptions);
      let panel = document.getElementById('panel');

      let direction = new google.maps.DirectionsRenderer({
        map : this.map,
        panel : panel
      });

      let request = {
        origin      : this.startPoint,
        destination : this.endPoint,
        travelMode  : google.maps.DirectionsTravelMode.WALKING,
        waypoints   : this.traces
      }

      let directionsService = new google.maps.DirectionsService();
      directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          direction.setDirections(response);
        }
      });

      direction.setMap(this.map);
    }
  }

  public loadMap(mapElement: ElementRef) {
    this.initMap(mapElement);
    this.getLocalisation();
  }

  public getLocalisation() {
    this.geolocation.watchPosition().subscribe(position => {
        console.log(position.coords.latitude + ' ' + position.coords.longitude);
        if (position != undefined) {
          let current_latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          if (typeof this.markerPosition == "undefined"){
            let icon = {
              path : google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
              scale : 5
            }
            this.markerPosition = new google.maps.Marker({
               map: this.map,
               animation: google.maps.Animation.DROP,
               position: current_latLng,
               icon : icon
             });
          } else{
            this.markerPosition.setPosition(current_latLng);
          }
        } else {
          console.log('Error Geolocation');
        }
    });
  }

}

import { Injectable } from '@angular/core';

// Model
import { Rando } from '../../entity/Rando/Rando';

// --------------------------------
// Service : RandoFactory

@Injectable()
export class RandoService {
    private rando : Rando;
    private timerActive : boolean;

    constructor() {
      this.timerActive = false
    }

    getRando(): Rando {
      return this.rando;
    }

    setRando(rando: Rando) {
      this.rando = rando;
    }

    setTimerActive(active: boolean) {
      this.timerActive = active;
    }

    getTimerActive(): boolean {
      return this.timerActive;
    }

    displayFooter(): boolean {
      return typeof this.rando !== "undefined" && this.timerActive;
    }
}

// Model
import { Rando } from '../../entity/Rando/Rando';
import { Point } from '../../entity/Point/Point';

// --------------------------------
// Service : RandoFactory

export class RandoFactory {

  public static construct(name: string, object): Rando {
    let rando = new Rando();

    rando
      .setName(name)
      .setLocality('Departement : ' + object.fields.departement)
      .setDescription("Métro-boulot…rando ? Au diable Pierre Béarn et le train-train parisien. Après une semaine agitée, rien de tel qu’une escapade en forêt pour s’aérer la tête. Aujourd’hui, on vous emmène marcher à deux pas de la capitale, le temps d’une journée.")
      .setTime("1H")
      .setHeightDiff((object.fields.st_length_shape/1000).toFixed(1) + " KM")
    ;

    object.fields.geo_shape.coordinates.forEach((data) => {
      let point = new Point();
      point
        .setLatitude(data[1])
        .setLongitude(data[0])
      ;
      rando.setPoint(point);
    });

    return rando;
  }

  public static generate(name: string) {
    let point_1 = new Point();
    point_1
    .setLatitude(45.763932)
    .setLongitude(2.955537)
    ;

    let point_2 = new Point();
    point_2
    .setLatitude(45.771256)
    .setLongitude(2.962405)
    ;

    let step_1 = new Point();
    step_1
    .setLatitude(45.767601)
    .setLongitude(2.959879)
    ;

    let step_2 = new Point();
    step_2
    .setLatitude(45.768632)
    .setLongitude(2.959421)
    ;

    let rando = new Rando();
    rando
    .setName(name)
    .setPoint(point_1)
    .setPoint(step_1)
    .setPoint(step_2)
    .setPoint(point_2)
    .setLocality("Puy de Dôme 63870 Orcines")
    .setDescription("Amoureux du grand air, la nature vous ouvre les bras. Découverte, culture, sport, ou détente... le volcan du puy de Dôme offre une palette d’activités pour toute la famille. Il surplombe l'ensemble des quelques 80 volcans de la Chaîne des Puys. ")
    .setTime("2H30")
    .setHeightDiff("300 M")
    ;

    return rando;
  }
}

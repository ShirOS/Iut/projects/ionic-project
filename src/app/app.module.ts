import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';

// Controller
import { HomePage } from '../pages/home/home';
import { ListRando } from '../pages/listRando/listRando';
import { DetailPage } from '../pages/detail/detail';
import { ActiveRando } from '../pages/activeRando/activeRando';

// Components
import { MapComponent } from '../component/map/map.component';
import { TimerComponent } from '../component/timer/timer.component';
import { FooterComponent } from '../component/footer/footer.component';

// Repository
import { RandoRepository } from '../repository/rando/randoRepository';

// Service
import { RandoService } from '../service/Rando/RandoService';
import { TimerService } from '../service/Timer/TimerService';
import { MapService } from '../service/Map/MapService';
import { HttpService } from '../service/Http/HttpService';

// Native Components
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,

    // Controllers
    HomePage,
    ListRando,
    DetailPage,
    ActiveRando,

    // Components
    MapComponent,
    TimerComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

    // Controllers
    HomePage,
    ListRando,
    DetailPage,
    ActiveRando,

    // Components
    MapComponent,
    TimerComponent,
    FooterComponent
  ],
  providers: [
    // Native Services
    StatusBar,
    SplashScreen,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},

    // My Repositories
    RandoRepository,

    // My Services
    RandoService,
    TimerService,
    MapService,
    HttpService
  ]
})
export class AppModule {}

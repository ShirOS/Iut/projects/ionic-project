// --------------------------------
// Model : Point

export class Point {
    private latitude:number;
    private longitude:number;

    constructor() {
        this.latitude = -1;
        this.longitude = -1;
    }

    // ----------------
    // Latitude

    setLatitude(latitude:number): Point {
        this.latitude = latitude;
        return this;
    }

    getLatitude(): number {
        return this.latitude;
    }

    // ----------------
    // Longitude

    setLongitude(longitude:number): Point {
        this.longitude = longitude;
        return this;
    }

    getLongitude(): number {
        return this.longitude;
    }
}

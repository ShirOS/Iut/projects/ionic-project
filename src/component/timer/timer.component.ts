import { Component } from '@angular/core';

// Services
import { TimerService } from '../../service/Timer/TimerService';

@Component({
    selector: 'Timer',
    templateUrl: 'timer.component.html'
})
export class TimerComponent {
  private timerService: TimerService;

  constructor(timerService: TimerService) {
    this.timerService = timerService;
    this.timerService.initTimer();
    this.timerService.startTimer();
  }

  // ----------------
  // Utils

  public hasStarted(): boolean {
    return this.timerService.hasStarted();
  }

  public hasPaused(): boolean {
    return this.timerService.hasPaused();
  }

  public initTimer() {
    this.timerService.initTimer();
  }

  public startTimer() {
    this.timerService.startTimer();
  }

  public pauseTimer() {
    this.timerService.pauseTimer();
  }

  public resumeTimer() {
    this.timerService.resumeTimer();
  }

  public resetTimer() {
    this.timerService.resetTimer();
  }

  // ----------------
  // Display

  public getDisplayTime(): String {
    return this.timerService.getDisplayTime();
  }
}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// Services
import { RandoService } from '../../service/Rando/RandoService';

//Controller
import { ActiveRando } from '../../pages/activeRando/activeRando';

// --------------------------------
// Component : Map
@Component({
  selector: 'Footer',
  templateUrl: 'footer.component.html'
})
export class FooterComponent {
  constructor( public navCtrl: NavController, public randoService: RandoService ) {}

  getRandoService() : RandoService{
    return this.randoService;
  }

  goToCurrentRando(event) {
    this.navCtrl.push(ActiveRando);
  }
}

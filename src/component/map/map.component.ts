import { Component, ViewChild, ElementRef } from '@angular/core';

// Model
import { Rando } from '../../entity/Rando/Rando';

// Services
import { MapService } from '../../service/Map/MapService';
import { RandoService } from '../../service/Rando/RandoService';

// --------------------------------
// Component : Map
@Component({
  selector: 'Map',
  templateUrl: 'map.component.html'
})
export class MapComponent {
  private rando: Rando;
  @ViewChild('map') private mapElement: ElementRef;

  constructor(public mapService: MapService, public randoService: RandoService) {
    this.rando = this.randoService.getRando();
  }

  ngOnInit() {
    if (typeof this.rando !== 'undefined') {
      this.mapService.prepareTraces(this.rando);
      this.mapService.loadMap(this.mapElement);
    }
  }
}
